## JsonFaucet interface description

### constructor( options )

#### Example
```
const fauct = new JsonFaucet( { indent: '  ' } );
faucet.pipe( fs.createWriteStream( 'mydump.json' ) );
```

JsonFaucet extends ```stream.Readable```. So all options of ```stream.Readable``` are forwarded.
Currently the only option directly affecting JsonFaucet is ```indent```.

In case ```indent``` is false (default) JsonFaucet will a stream as compact as possible
In case ```indent``` is a string, it will prepended n times for each indent level.
If indent is not false a space is added before and after every ```:``` used by attributes.

If indent contains any characters other than space or tab,
the resulting stream will not be valid JSON.


### async beginDocument( )

#### Example
```
await faucet.beginDocument( );
```

Begins a JSON "document". Emits a ```{``` onto the stream.
Must be the first call after construction.


### async endDocument( suffix )

#### Example
```
await faucet.endDocument( '\n' );
```

Ends a JSON "document". Emits a ```}``` onto the stream.
If suffix is not undefined, it is appended after the closing bracket.
Intention is to be used as final newline.


### async attribute( name, val )

#### Example
```
await faucet.beginDocument( );
await faucet.attribute( 'foo', 'bar' );
await faucet.endDocument( );
```

```name``` has to be a string and dennotes the name of the attribute.
```val``` is optional, it can be used to specify a value of the attribute right away.

```val``` will be stringified with ```JSON.stringify( )```. Note that if it is an Object or Array and ```indent``` is not ```false``` indentation will not be respected for value.


### async value( val )

#### Example 1
```
await faucet.beginDocument( );
await faucet.attribute( 'foo' );
await faucet.value( 'bar' );
await faucet.endDocument( );
```

#### Example 2
```
await faucet.beginDocument( );
await faucet.attribute( 'myArray' );
await faucet.beginArray( );
await faucet.value( 'foo' );
await faucet.value( 'bar' );
await faucet.endArray( );
await faucet.endDocument( );
```

Writes a value onto the JSON stream. Must come either after ```attribute( )``` without a value given or within an Array.

### async beginObject(  )

#### Example 1
```
await faucet.beginDocument( );
await faucet.attribute( 'myObject' );
await faucet.beginObject( );
await faucet.attribute( 'foo', 'bar' );
await faucet.endObject( );
await faucet.endDocument( );
```

#### Example 2
```
await faucet.beginDocument( );
await faucet.attribute( 'myArray' );
await faucet.beginArray( );
await faucet.beginObject( );
await faucet.value( 'foo', 'bar' );
await faucet.endObject( );
await faucet.endArray( );
await faucet.endDocument( );
```

Starts an Object. Emits a ```{``` onto the stream. Must come either after ```attribute( )``` without a value given or within an Array.

### async endObject(  )

#### Example 1
```
await faucet.beginDocument( );
await faucet.attribute( 'myObject' );
await faucet.beginObject( );
await faucet.attribute( 'foo', 'bar' );
await faucet.endObject( );
await faucet.endDocument( );
```

Ends an Object. Emits a ```}``` onto the stream. Must match a ```beginObject( )``` call.

###  async beginArray( )

#### Example
```
await faucet.beginDocument( );
await faucet.attribute( 'myArray' );
await faucet.beginArray( );
await faucet.value( 'foo' );
await faucet.value( 1 );
await faucet.value( { 'bar' : 2 } );
await faucet.endArray( );
await faucet.endDocument( );
```

Starts an Array. Emits a ```[``` onto the stream. Must come either after ```attribute( )``` without a value given or within an Array to cascade arrays.

###  async endArray( )

#### Example
```
await faucet.beginDocument( );
await faucet.attribute( 'myArray' );
await faucet.beginArray( );
await faucet.beginArray( );
await faucet.value( 1 );
await faucet.endArray( );
await faucet.endArray( );
await faucet.endDocument( );
```

Ends an Array. Emits a ```]``` onto the stream. Must match a ```beginArray( )``` call.

### Error handling

In case calls are not properly matched an error is thrown.

### Flow control

All async functions will pause he current flow in case the stream hits the high watermark
and will resumt if it get's emptied by it's pipe.

The high watermark is signaled by the underlying ```stream.Readable``` implementation with ```push( )``` returning false.
