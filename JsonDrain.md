## JsonDrain interface description

### constructor( stream, options )

#### Example
```
const stream = fs.createReadStream( 'mydump.json' );
const drain = new JsonDrain( stream );
```

Creates an asynchronous parsing drain. ```stream``` is a readable stream used as source.
JsonDrain keeps it's own buffer for chunks received from ```stream-json```.
Currently only recognized options for ```options``` is ```maxBuf```.
If the buffer reaches the high mark of ```maxBuf``` Json-Drain pauses the input stream.

Currently default for ```maxBuf``` is 1.


JsonFaucet extends ```stream.Readable```. So all options of ```stream.Readable``` are forwarded.
Currently the only option directly affecting JsonFaucet is ```indent```.

In case ```indent``` is false (default) JsonFaucet will a stream as compact as possible
In case ```indent``` is a string, it will prepended n times for each indent level.
If indent is not false a space is added before and after every ```:``` used by attributes.

If indent contains any characters other than space or tab,
the resulting stream will not be valid JSON.


### async next( )

Returns the next ```"chunk"``` from the stream.

#### Example
```
while( true )
{
	const chunk = await drain.next( );
	if( chunk.object === 'end' ) break;
	// further handling here
}
```

A "chunk" is an object with the possible attributes ```array```, ```attribute```, ```object``` and ```value``` set.
Combinations of these may be possible.

The chunk creation is best explained with a series of examples:

For example following JSON code:
```
"version" : 1
```
will result in a chunk
```
{ attribute: 'version', value: 1 },
```

JSON code:
```
"letters" : { "a" : 1, "b" : 2 }
```
yields following chunks (with consecutive calls of ```next( )```)
```
{ attribute: 'letters', object: 'start' }
{ attribute: 'a', value : 1 }
{ attribute: 'b', value : 2 }
{ object: 'end' }
```

JSON code:
```
"digits" : [ 1, 2, 3 ]
```
yields following chunks (with consecutive calls of ```next( )```)
```
{ attribute: 'digits', array: 'start' }
{ array: 'value', value : 1 }
{ array: 'value', value : 2 }
{ array: 'value', value : 3 }
{ array: 'end' }
```

JSON code:
```
"pairs" : [ [ 11, 12 ], [ 21, 22 ] ]
```
yields following chunks (with consecutive calls of ```next( )```)
```
{ attribute: 'pairs', array: 'start' }
{ array: 'cascade' }
{ array: 'value', value : 11 }
{ array: 'value', value : 12 }
{ array: 'end' }
{ array: 'cascade' }
{ array: 'value', value : 21 }
{ array: 'value', value : 22 }
{ array: 'end' }
{ array: 'end' }
```

JSON code:
```
"points" : [ { "x": 1, "y" : 1 }, { "x": 5, "y" : 8 } ]
```
yields following chunks (with consecutive calls of ```next( )```)
```
{ attribute: 'points', array: 'start' }
{ array: 'value', 'object': 'start' }
{ attribute: 'x', value : 1 }
{ attribute: 'y', value : 1 }
{ object: 'end' }
{ array: 'value', 'object': 'start' }
{ attribute: 'x', value : 5 }
{ attribute: 'y', value : 8 }
{ object: 'end' }
{ array: 'end' }
```

If the input stream ends following chunk will be returned by ```ǹext( )```
```
{ endofstream: true }
```

### async retrieve( )

Retrieves the current object or array as whole building it in memory.
Must be called only after ```{array:'begin'}```, ```{array:'cascade'}```
or ```{object:begin}```.

Only sensible to be called if the current object or array
is not expected to exceed memory.

### async skip( )

Skips the current object or array.
Must be called only after ```{array:'begin'}```, ```{array:'cascade'}```
or ```{object:begin}```.

Sensible to be called even if the current object or array
is expected to exceed memory.

### Flow control

All async functions will pause current flow control until enough JSON data received
on the input stream to emit the next chunk or in case of ```retrieve( )``` or ```skip( )```
the current object or array is finished.

