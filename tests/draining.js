/*
| Simply prints all the chunks received by json on stdin.
*/
const { JsonDrain } = require( '..' );

async function run( )
{
	const drain = new JsonDrain( process.stdin );
	while( true )
	{
		const chunk = await drain.next( );
		console.log( chunk );
		if( chunk.endofstream ) break;
	}
}

process.on( 'unhandledRejection', err => { throw err; } );
run( );
