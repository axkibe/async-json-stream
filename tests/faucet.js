/*
| Creates an example json.dump
*/

const fs = require( 'fs' );
const { JsonFaucet } = require( '..' );

const run =
	async function( )
{
	const faucet = new JsonFaucet( { indent : '  ' } );
	faucet.pipe( fs.createWriteStream( 'mydump.json' ) );
	await faucet.beginDocument( );
	await faucet.attribute( 'version', 1 );

	await faucet.attribute( 'vocals' );
	await faucet.beginObject( );
	for( let v of [ 'a', 'e', 'i', 'o', 'u' ] )
	{
		await faucet.attribute( v );
		await faucet.beginObject( );
		await faucet.attribute( 'charCode', v.charCodeAt( 0 ) );
		await faucet.endObject( );
	}
	await faucet.endObject( );

	await faucet.attribute( 'digits' );
	await faucet.beginArray( );
	for( let d = 0; d < 10; d++ )
		await faucet.value( d );
	await faucet.endArray( );
	await faucet.endDocument( );
};

process.on( 'unhandledRejection', err => { throw err; } );
run( );
