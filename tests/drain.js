/*
| Parses through the JSON dump created by tests/faucet.js
*/

const fs = require( 'fs' );
const { JsonDrain } = require( '..' );

function jerror( ) { throw new Error( 'invalid JSON dump' ); }

async function run( )
{
	let digits, version, vocals;

	const stream = fs.createReadStream( 'mydump.json' );
	const drain = new JsonDrain( stream );
	const msg = 'invalid JSON dump';

	const start = await drain.next( );
	if( start.object !== 'start' ) throw new Error( msg );

	while( true )
	{
		const chunk = await drain.next( );
		if( chunk.object === 'end' ) break;
		switch( chunk.attribute )
		{
			case 'version' :
				if( version ) jerror( );
				version = chunk.value;
				if( !version ) jerror( );
				continue;
			case 'vocals' :
				if( vocals ) jerror( );
				if( chunk.object !== 'start' ) jerror( );
				vocals = await handleVocals( drain );
				continue;
			case 'digits' :
				if( digits ) jerror( );
				if( chunk.array !== 'start' ) jerror( );
				digits = await handleDigits( drain );
				continue;
			default : jerror( );
		}
	}

	if( !version || !vocals || !digits ) jerror( );
	console.log( '* done' );
}

async function handleVocals( drain )
{
	while( true )
	{
		const chunk = await drain.next( );
		if( chunk.object === 'end' ) return true;
		const letter = chunk.attribute;
		if( chunk.object !== 'start' ) jerror( );
		const entry = await drain.next( );
		if( entry.attribute !== 'charCode' ) jerror( );
		console.log( '* got vocal "' + letter + '" having charCode: ' + entry.value );
		// awaiting end of entry
		const eoe = await drain.next( );
		if( eoe.object !== 'end' ) jerror( );
	}
}

async function handleDigits( drain )
{
	while( true )
	{
		const chunk = await drain.next( );
		if( chunk.array === 'end' ) return true;
		const digit = chunk.value;
		if( typeof( digit ) !== 'number' ) jerror( );
		console.log( '* got digit: ' + digit );
	}
}

process.on( 'unhandledRejection', err => { throw err; } );
run( );
